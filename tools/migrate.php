<?php

if (!empty($_SERVER["HTTP_HOST"])){
    die('console only');
}

set_time_limit(0);
error_reporting(E_ERROR );

define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC","Y");
define("NOT_CHECK_PERMISSIONS", true);

$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
// flush all buffers
while (ob_get_level())
    ob_end_flush();
    
if (\CModule::IncludeModule('sprint.migration')){
    $console = new Sprint\Migration\Console();
    $console->execFromArgs($argv);
} else {
    echo 'need to install module sprint.migration';
}



require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");

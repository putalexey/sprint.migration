<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 11.10.2017 18:51
 */

namespace Sprint\Migration\Helpers;


/**
 * Class SiteTemplateBuilder
 *
 * @method $this setSort($value)
 * @method $this setCondition($value)
 * @method $this setTemplate($value)
 * @method string getSort()
 * @method string getCondition()
 * @method string getTemplate($value)
 *
 * @package Sprint\Migration\Helpers
 */
class SiteTemplateBuilder extends BaseBuilder
{
    protected $params = [
        "SORT" => "100",
        "CONDITION" => "",
        "TEMPLATE" => "",
    ];
}
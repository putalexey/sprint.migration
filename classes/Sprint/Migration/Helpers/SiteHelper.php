<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 11.06.2015 17:27
 */

namespace Sprint\Migration\Helpers;

class SiteHelper
{
    public $site = null;

    public function __construct()
    {
        $this->obSite = new \CSite();
        $this->site = $this->obSite->GetDefSite();
    }

    public function getSite($site)
    {
        $rs = $this->obSite->GetList($by="lendir", $order="desc", ['LID' => $site]);
        return $rs->Fetch();
    }

    public function addSiteIfNotExists($arSite)
    {
        if (!$this->getSite($arSite["LID"])) {
            $this->addSite($arSite);
        }
    }

    public function deleteSite($SITE_ID)
    {
        return $this->obSite->Delete($SITE_ID);
    }

    /**
     * NAME
     * LID
     * DIR
     * LANGUAGE_ID
     * CULTURE_ID
     * SORT
     * TEMPLATE
     * @param $arSite
     */
    public function addSite($arSite)
    {
        return $this->obSite->Add($arSite);
    }

    public function setSite($site)
    {
        $this->site = $site;
    }

    public function selectSite($LID)
    {
        $arSite = $this->getSite($LID);
        $this->site = $arSite["LID"];
    }

    public function getTemplates()
    {
        $rs = $this->obSite->GetTemplateList($this->site);
        $templates = array();
        while($tmpl = $rs->Fetch()) {
            $templates[] = $tmpl;
        }

        return $templates;
    }

    public function addBatchTemplates($callback)
    {
        $templates = $this->getTemplates();

        $newTemplates = [];
        if (is_callable($callback)) {
            $newTemplates = call_user_func($callback, $templates);
        } else if(is_array($callback) && count($callback) > 0) {
            if (array_key_exists('CONDITION', $callback))
                $newTemplates = [$callback];
            else
                $newTemplates = $callback;
        }
        if ($newTemplates) {
            $templates = array_merge($templates, $newTemplates);

            $this->setTemplates($templates);
            
            return true;
        }
        return false;
    }
    
    /**
     * $templateParams example
     * array(
     *     "CONDITION" => "CSite::InDir('/auth/index.php')",
     *     "SORT" => "100",
     *     "TEMPLATE" => "brainify_payment",
     * )
     * @param $templateParams
     */
    public function addTemplate($templateParams)
    {
        $templates = $this->getTemplates();
        $templates[] = $templateParams;

        $this->setTemplates($templates);
    }
    
    public function deleteTemplate($conditions)
    {
        $templates = $this->getTemplates();
        $filteredTemplates = array_filter($templates, function($tmpl) use ($conditions) {
            foreach($conditions as $field => $value) {
                if ($value != $tmpl[$field]) {
                    return true;
                }
            }
            return false;
        });
        if(count($filteredTemplates) != count($templates)) {
            $this->setTemplates($filteredTemplates);
        }
    }

    protected function setTemplates($templates)
    {
        $this->obSite->Update($this->site, array(
            "TEMPLATE" => $templates,
        ));
    }
}
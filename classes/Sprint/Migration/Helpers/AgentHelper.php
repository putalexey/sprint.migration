<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 11.06.2015 17:27
 */

namespace Sprint\Migration\Helpers;


use Bitrix\Main\Type\DateTime;

class AgentHelper
{
    public function addAgentIfNotExists($fields)
    {
        if($arAgent = self::getAgent($fields["NAME"])) {
            return $arAgent;
        } else {
            return self::addAgent($fields);
        }
    }

    public function addAgent($fields)
    {
        global $DB;

        if ($fields["NEXT_EXEC"] instanceof \DateTime ||
            $fields["NEXT_EXEC"] instanceof DateTime
        ) {
            $dateString = $fields["NEXT_EXEC"]
                ->format($DB->DateFormatToPHP(\CLang::GetDateFormat("FULL", "ru")));
            $fields["NEXT_EXEC"] = $dateString;
        }
        $arFields = array_merge(Array(
            "NAME" => '',
            "MODULE_ID" => "main",
            "ACTIVE" => "N",
            "SORT" => 100,
            "IS_PERIOD" => "N",
            "AGENT_INTERVAL" => 60 * 60 * 24,
//            "NEXT_EXEC" => '',
            "USER_ID" => false
        ), $fields);
        $ob = new \CAgent;
        $res = $ob->Add($arFields);
        if ($res) {
            $arFields["ID"] = $res;
            return $arFields;
        }
        return false;
    }

    public function deleteAgent($cmd)
    {
        $arAgent = self::getAgent($cmd);
        if($arAgent) {
            $ob = new \CAgent;
            return $ob->Delete($arAgent["ID"]);
        }
        return true;
    }

    public function updateAgent($fields)
    {
        $arAgent = self::getAgent($fields["NAME"]);
        if($arAgent) {
            $ob = new \CAgent;
            return $res = $ob->Update($arAgent["ID"], $fields);
        }
        return false;
    }

    public function getAgent($cmd)
    {
        $arAgent = \CAgent::GetList(array(), array(
            "NAME" => $cmd
        ))->Fetch();
        return $arAgent;
    }

    public function getAgentById($id)
    {
        return \CAgent::GetByID($id)->Fetch();
    }
}
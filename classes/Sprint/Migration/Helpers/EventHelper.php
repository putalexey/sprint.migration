<?php

namespace Sprint\Migration\Helpers;

class EventHelper
{
    public function getEventType($eventName, $LID = 'ru') {
        $rs = \CEventType::GetList(['EVENT_NAME' => $eventName, 'LID' => $LID]);
        return $rs->Fetch();
    }

    public function addEventType($eventName, $fields) {
        $default = array(
            "LID" => 'ru',
            "EVENT_NAME" => 'EVENT_NAME',
            "NAME" => 'Название',
            "DESCRIPTION" => 'Описание',
            'SORT' => '',
        );

        $fields = array_merge($default, $fields);
        $fields['EVENT_NAME'] = $eventName;

        $event = new \CEventType;
        $id = $event->Add($fields);
        return $id;
    }

    public function deleteEventType($eventName, $filter = []) {
        $defaults = array(
            "EVENT_NAME" => $eventName,
        );
        $fields = array_merge($defaults, $filter);
        \CEventType::Delete($fields);

        return true;
    }


    public function addEventMessage($eventName, $fields) {
        $default = array(
            'ACTIVE' => 'Y',
            'LID' => 's1',
            'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
            'EMAIL_TO' => '#EMAIL_TO#',
            'BCC' => '',
            'SUBJECT' => 'Тема сообщения',
            'BODY_TYPE' => 'text',
            'MESSAGE' => 'Текст сообщения',
        );

        $fields = array_merge($default, $fields);
        $fields['EVENT_NAME'] = $eventName;

        $event = new \CEventMessage;
        $id = $event->Add($fields);

        echo $event->LAST_ERROR;
        return $id;
    }

    public function deleteEventMessages($eventName, $filter = []) {
        $defaults = array(
            "EVENT_NAME" => $eventName,
        );
        $fields = array_merge($defaults, $filter);
        $rs = \CEventMessage::GetList($by="ID", $order="ASC", $fields);
        while ($ar = $rs->Fetch()) {
            \CEventMessage::Delete($ar["ID"]);
        }

        return true;
    }

    public function updateEventMessageByFilter($filter, $fields) {

        $event = new \CEventMessage;

        $eventList = $event->GetList($by = "site_id", $order = "desc", $filter);

        while ($mess = $eventList->getNext()) {
            if (!$event->Update($mess["ID"], $fields)) {
                echo $event->LAST_ERROR . "\n";
            }
        }

    }

    public function updateEventMessage($eventName, $fields) {

        if (!is_array($eventName)) {
            $filter = array(
                "TYPE_ID" => $eventName,
            );
        } else {
            $filter = $eventName;
        }
        $this->updateEventMessageByFilter($filter, $fields);
    }

}
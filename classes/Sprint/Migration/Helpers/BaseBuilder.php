<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 11.10.2017 18:51
 */

namespace Sprint\Migration\Helpers;


use Illuminate\Support\Str;

class BaseBuilder
{
    protected $params = [];

    public static function getInstance($params = []) {
        return new static($params);
    }
    public static function getCleanInstance() {
        $inst = new static();
        $inst->clearParams();
        return $inst;
    }

    public function __construct($params = [])
    {
        $this->params = array_merge($this->params, $params);
    }

    public function clearParams()
    {
        $this->params = [];
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed|$this
     */
    public function __call($name, $arguments)
    {
        if (strpos($name, 'set') === 0) {
            $field = mb_strtoupper(Str::snake(substr($name, 3)));

            $this->params[$field] = $arguments[0];
            return $this;
        }
        if (strpos($name, 'get') === 0) {
            $field = mb_strtoupper(substr($name, 3));
            return $this->params[$field];
        }
        throw new \BadMethodCallException("Function $name not implemented in " . static::class);
    }

    public function __set($name, $value)
    {
        $this->params[$name] = $value;
    }

    public function __get($name)
    {
        return $this->params[$name];
    }

    public function get() {
        return $this->params;
    }
}